import cleanspeech, datetime, findwiki, pyttsx, re, speech_recognition, textwrap, time, weather

speak = pyttsx.init()
speak.setProperty('voice', 'com.apple.speech.synthesis.voice.Alex')

recog = speech_recognition.Recognizer()

def listen():
  with speech_recognition.Microphone() as source:
    recog.pause_threshold = 1
    recog.adjust_for_ambient_noise(source)
    #print 'mic calibrated'
    print '-' * 21
    audio = recog.listen(source)  
  try:
    return recog.recognize_google(audio)
  except speech_recognition.UnknownValueError:
    return "Couldn't Understand"
  except speech_recognition.RequestError as e:
    return("Recog Error; {0}".format(e))
  return ''

print '-' * 21
print 'Jarvis - Starting Up'

while True:

  response = listen()
  print datetime.datetime.now().strftime('%D - %I:%M %p') + ' : ' + response

  if 'what' in response.split() or "what's" in response.split() or 'how' in response.split() or "how's" in response.split():
    if 'weather' in response.split():
      print '-' * 21
      try:
        wrText = weather.getForecast(response)
        print textwrap.fill(wrText[0] + ' - ' + wrText[1])
        speak.say(cleanspeech.cleanWeather(wrText[1]))
        speak.runAndWait()
      except:
        speak.say("Jarvis - I can't report the weather right now because I'm dumb")
        speak.runAndWait()

  if 'what time is it' in response:
    speak.say(datetime.datetime.now().strftime('%I:%M %p'))
    speak.runAndWait()

  if 'start' in response.split() or 'begin' in response.split() and 'Wikipedia' in response.split():
    try:
      print '-' * 21
      print 'Jarvis - What do you want to know about?'
      speak.say('What do you want to know about?')
      speak.runAndWait()
      while True:
        newResponse = listen()
        print datetime.datetime.now().strftime('%D - %I:%M %p') + ' : ' + newResponse
        print '-' * 21
        if 'exit Wikipedia' in newResponse or 'quit Wikipedia' in newResponse or 'stop Wikipedia' in newResponse:
          print 'Jarvis - Exiting Wikipedia'
          speak.say('Exiting Wikipedia')
          speak.runAndWait()
          break
        try:
          wikiText = findwiki.lookUp(newResponse.lower())
          print textwrap.fill(wikiText[0] + ' - ' + wikiText[1])
          speak.say(wikiText[1])
          speak.runAndWait()
        except speech_recognition.UnknownValueError:
          print "Couldn't Understand"
          print '-' * 21
          pass
        except:
          print "I'm having trouble finding that subject"
          pass
    except:
      speak.say("I've temporarily lost my eyesight and can't read wikipedia right now")
      speak.runAndWait()

  if 'what' in response.split() and 'day' in response.split() or 'date' in response.split() and 'today' in response.split():
    speak.say(datetime.datetime.now().strftime('%A, %B %d, %Y'))
    speak.runAndWait()

  if 'end voice control' in response or 'stop voice control' in response or 'exit voice control' in response or 'quit voice control' in response or 'shutdown Jarvis' in response:
    print '-' * 21
    print 'Jarvis - Shutting Down'
    print '-' * 21
    speak.say('Ending Voice Control')
    speak.runAndWait()
    break

  if 'take a break' in response:
    speak.say('Taking a 30 minute break, restart script to resume voice commands earlier than that')
    speak.runAndWait()
    time.sleep(1 * 60 * 30)
    speak.say('Ready for command')
    speak.runAndWait()
