import json, urllib2
from datetime import datetime

def getForecast(when):
  try:
    api = 'API_KEY'
    zipcode = 'ZIPCODE'
    url = 'http://api.wunderground.com/api/' + api + '/forecast10day/q/' + zipcode + '.json'
    urlOpen = urllib2.urlopen(url)
    rawForecast = json.loads(urlOpen.read())
    wantedForecast = rawForecast['forecast']['txt_forecast']['forecastday'][:14]

    if 'now' in when.split():
      url = 'http://api.wunderground.com/api/' + api + '/conditions/q/' + zipcode + '.json'
      urlOpen = urllib2.urlopen(url)
      rawForecast = json.loads(urlOpen.read())
      currentWeather = str(rawForecast['current_observation']['weather'] + ' with a temperature of ' + str(rawForecast['current_observation']['temp_f']) + ' degrees.')
      #print datetime.now().strftime('%D - %I:%M %p'),'-',currentWeather
      return [datetime.now().strftime('%D - %I:%M %p'),currentWeather]

    if 'today' in when.split() or "today's" in when.split():
      #print datetime.now().strftime('%A Night')
      for x in wantedForecast:
        if x['title'].lower() == str(datetime.now().strftime('%A')).lower():
          #print x['title'],'-',x['fcttext']
          return [x['title'],x['fcttext']]


    if 'tonight' in when.split() or "tonight's" in when.split():
      #print datetime.now().strftime('%A Night')
      for x in wantedForecast:
        if x['title'].lower() == str(datetime.now().strftime('%A Night')).lower():
          #print x['title'],'-',x['fcttext']
          return [x['title'],x['fcttext']]

    if 'tomorrow' in when.split() or "tomorrow's" in when.split():
      today = datetime.now().strftime('%D')
      tomorrowMath = today[:3] + str(int(today[3:5]) + 1) + today[5:]
      tomorrowMath2 = datetime.strptime(tomorrowMath, '%m/%d/%y')
      if 'night' in when.split() or "night's" in when.split():
        forecastFor = tomorrowMath2.strftime('%A Night')
      else:
        forecastFor = tomorrowMath2.strftime('%A')
      for x in wantedForecast:
        if x['title'].lower() == forecastFor.lower():
          #print x['title'],'-',x['fcttext']
          return [x['title'],x['fcttext']]

    i = 0
    for x in wantedForecast:
      if x['title'].lower() in when.lower().split() or str(x['title'].lower() + "'s") in when.lower().split():
        if 'night' in when.lower().split() or "night's" in when.lower().split():
          #print wantedForecast[i + 1]['title'],'-',wantedForecast[i + 1]['fcttext']
          return [wantedForecast[i + 1]['title'],wantedForecast[i + 1]['fcttext']]
        else:
          #print x['title'],'-',x['fcttext']
          return [x['title'],x['fcttext']]
      i += 1
      #print x['title'],'-',x['fcttext']
      #print '-' * 20

    else:
      url = 'http://api.wunderground.com/api/' + api + '/conditions/q/' + zipcode + '.json'
      urlOpen = urllib2.urlopen(url)
      rawForecast = json.loads(urlOpen.read())
      conditions = str(rawForecast['current_observation']['weather'])
      temp = str(rawForecast['current_observation']['temp_f'])
      winds = str('Winds are ' + rawForecast['current_observation']['wind_string']).replace('MPH','miles per hour')
      currentWeather = str(conditions + ' with a temperature of ' + temp + ' degrees. ' + winds)
      #print datetime.now().strftime('%D - %I:%M %p'),'-',currentWeather
      return [datetime.now().strftime('%D - %I:%M %p'),currentWeather]

  except:
    print datetime.now().strftime('%I:%M %p - '), 'Failed to get Weather Forecast'