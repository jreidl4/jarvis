import textwrap, wikipedia
from summa import summarizer, keywords

"""
print '-' * 20
text = wikipedia.summary('ancient rome')
print textwrap.fill(summarizer.summarize(text))
print '-' * 20
print textwrap.fill(text)
print '-' * 20
"""

#questionWords = ['look','up','search','for','find','who','what','where','when','is','was','were']
#this stuff will be fore better context processing

def lookUp(subject):

  if subject == 'exit voice control' or subject == "couldn't understand":
    pass

  else:
    try:
      wiki = wikipedia.page(subject)
      if ' a b c ' in wiki.summary:
        summary = summarizer.summarize(wiki.summary[:wiki.summary.find(' a b c ') - 2])
      if '^ ' in wiki.summary:
        summary = summarizer.summarize(wiki.summary[:wiki.summary.find('^ ') - 1])
      else:
        summary = summarizer.summarize(wiki.summary,words=150)
      #print wiki.title,'-',summary
      return [wiki.title,summary]
    except:
      return ''
