import re

def cleanWeather(badstuff):

  try:
    cardinals = {'E':'east', 'ENE':'east northeast', 'ESE':'east southeast', 'NE':'northeast', 'NNE':'north northeast', 'N':'north', 'NNW':'north northwest', 'NW': 'northwest', 'SE':'southeast', 'SSE':'south southeast', 'S':'south', 'SSW':'south southwest', 'SW':'southwest', 'W':'west', 'WNW':'west northwest', 'WSW':'west southwest'}
    mph = 'miles per hour'
    goodstuff = badstuff

    for x in cardinals:
      if x in badstuff.split():
        goodstuff = badstuff.replace(x, cardinals[x])
    for x in goodstuff.split():
      if 'mph' in x:
        goodstuff = goodstuff.replace('mph', mph)
    if re.search("\d\dF", goodstuff):
      temp = re.search("\d\dF", goodstuff).group()
      newTemp = temp.replace('F', ' degrees')
      goodstuff = re.sub("\d\dF", newTemp, goodstuff)
    if 'precip' in goodstuff.split():
      goodstuff = goodstuff.replace('precip','precipitation')
    if 'winds' in goodstuff.lower().split():
      goodstuff = goodstuff.replace('winds','wynds')
    return goodstuff
  except:
    return badstuff
